package dev.brandonzx3;

public class PidController {
    private double pGain;
    private double iGain;
    private double dGain;

    private double errLast = 0;
    private boolean derivativeInitalized;

    private double integrationStored;

    private double outputMin;
    private double outputMax;

    public PidController(double p, double i, double d, double outputMin, double outputMax) {
        pGain = p;
        iGain = i;
        dGain = d;
        this.outputMin = outputMin;
        this.outputMax = outputMax;
    }

    public double Update(double dt, double currentValue, double targetValue) {
        double err = targetValue - currentValue;

        //calculate P term
        double p = pGain * err;

        //calculate I term
        integrationStored = integrationStored + (err * dt);
        double i = iGain * integrationStored;

        //cauculate D term
        double errRateOfChange = (err - errLast) / dt;
        errLast = err;
        double d = 0;
        if(derivativeInitalized) {
            d = dGain * errRateOfChange;
        } else {
            derivativeInitalized = true;
        }

        double result = p + i + d;

        return clamp(result, outputMin, outputMax);
    }

    public void SetGains(double p, double i, double d) {
        pGain = p;
        iGain = i;
        dGain = d;
    }

    private double clamp(double value, double min, double max) {
        return Math.max(min, Math.min(max, value));
    }
}