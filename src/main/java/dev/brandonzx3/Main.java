package dev.brandonzx3;

import com.pi4j.Pi4J;

import org.photonvision.PhotonCamera;

//NECESSARY PRECOMPILED LIBRARIES:
//https://frcmaven.wpi.edu/ui/native/wpilib-mvn-release-local/edu/wpi/first/hal/hal-jni/2022.4.1
//https://frcmaven.wpi.edu/ui/native/wpilib-mvn-release-local/edu/wpi/first/ntcore/ntcore-jni/2022.4.1/
//THEY NEED TO BE ON THE PATH VARIABLE (I think -christian)
//on RPI place those in /usr/lib because the root project folder isn't in the search path

import edu.wpi.first.networktables.NetworkTableInstance;

public final class Main {
  public static void main(String... args) throws InterruptedException {
    PidController yawController = new PidController(0, 0, 0, -1, 1);
    PidController pitchController = new PidController(0, 0, 0, -1, 1);

    var pi4j = Pi4J.newAutoContext(); 
    Motor yawMotor = new Motor(pi4j, 13, 5, 6, "yaw");
    Motor pitchMotor = new Motor(pi4j, 19, 16, 20, "pitch");

    NetworkTableInstance instance = NetworkTableInstance.getDefault();
    instance.startClient("127.0.0.1");
    Thread.sleep(2000);
    PhotonCamera cam = new PhotonCamera(instance, "camcam");

    long lastTime = System.nanoTime();
    while (true) {
      long time = System.nanoTime();
      int deltaTime = (int) ((time - lastTime) / 1000000);
      lastTime = time;
      
      if(cam.getLatestResult() != null) {
        if(cam.getLatestResult().hasTargets()) {
          if(cam.getLatestResult().getBestTarget() != null) {
            double yaw = cam.getLatestResult().getBestTarget().getYaw();
            double pitch = cam.getLatestResult().getBestTarget().getPitch();

            System.out.println("yaw: " + yaw + " pitch: " + pitch);
          
            double horizontalMovement = yawController.Update(deltaTime, yaw, 0) * 100;
            double verticalMovenrnt = pitchController.Update(deltaTime, pitch, 0) * 100;
            yawMotor.Run((int)horizontalMovement);
            pitchMotor.Run((int)verticalMovenrnt);
          } else {
            System.out.println("the target is null");
          }
        } else {
          System.out.println("no target");
          yawMotor.Stop();
          pitchMotor.Stop();
        }
      } else {
        System.out.println("WHATTT????");
      }
      Thread.sleep(50);
    }
  }
}
