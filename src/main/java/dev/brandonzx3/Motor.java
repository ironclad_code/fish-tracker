package dev.brandonzx3;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.pwm.Pwm;
import com.pi4j.io.pwm.PwmConfig;
import com.pi4j.io.pwm.PwmType;

public class Motor {
    private Pwm pwm;
    private DigitalOutput clockwisePin;
    private DigitalOutput counterClockwisePin;

    public Motor(Context pi4j, int address, int clockwisePin, int counterClockwisePin, String name) {
        pi4j = Pi4J.newAutoContext();
        pwm = pi4j.create(buildPwmConfig(pi4j, address, name));
        Stop();
        this.clockwisePin = pi4j.dout().create(clockwisePin);
        this.counterClockwisePin = pi4j.dout().create(counterClockwisePin);
    }

    public void Run(int speed) {
        if(speed > 0) {
            clockwisePin.low();
            counterClockwisePin.high();
        } else {
            clockwisePin.high();
            counterClockwisePin.low();
        }
        pwm.on(speed);
    }

    public void Stop() {
        pwm.off();
    }

    private static PwmConfig buildPwmConfig(Context pi4j, int address, String name) {
        return Pwm.newConfigBuilder(pi4j)
            .id("BCM" + address)
            .name(name)
            .address(address)
            .pwmType(PwmType.HARDWARE)
            .provider("pigpio-pwm")
            .initial(0)
            .shutdown(0)
            .build();
    }
}
